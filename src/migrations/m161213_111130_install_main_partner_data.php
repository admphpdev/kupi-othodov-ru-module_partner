<?php

use amd_php_dev\yii2_components\migrations\generators\Page;
use amd_php_dev\yii2_components\migrations\Migration;

class m161213_111130_install_main_partner_data extends Migration
{
    public static $partnerTableName = '{{%partner_partner}}';

    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
        $generator = new Page($this, self::$partnerTableName);
        $generator->create();
    }

    public function safeDown()
    {
        $this->dropTable(self::$partnerTableName);
    }
}
