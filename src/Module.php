<?php

namespace kupi_othodov_ru\module_partner;

/**
 * partner module definition class
 */
class Module extends \amd_php_dev\yii2_components\modules\Module
{
    //public $layout      = '@app/views/layouts/default';

    use \amd_php_dev\yii2_components\modules\ComposerModuleTrait;

    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'kupi_othodov_ru\module_partner\controllers';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        $this->modules = [
            'admin' => [
                'class' => 'kupi_othodov_ru\module_partner\modules\admin\Module',
            ],
        ];

        // custom initialization code goes here
    }

    //public static function getMenuItems() {
    //    return [
    //        'section' => 'partner',
    //        'items' => [
    //            [
    //                'label' => 'partner',
    //                'items' => [
    //                    ['label' => 'label', 'url' => ['']],
    //                ]
    //            ]
    //        ],
    //    ];
    //}
}
