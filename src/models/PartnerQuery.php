<?php

namespace kupi_othodov_ru\module_partner\models;

/**
 * This is the ActiveQuery class for [[Partner]].
 *
 * @see Partner
 */
class PartnerQuery extends \amd_php_dev\yii2_components\models\PageQuery
{

    /**
    * @inheritdoc
    */
    public function behaviors()
    {
        //return ArrayHelper::merge(parent::behaviors(), [
        //
        //]);
        return parent::behaviors();
    }

    /**
     * @inheritdoc
     * @return Partner[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return Partner|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
